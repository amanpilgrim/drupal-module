<?php

namespace Drupal\archimedes_client\Controller;

use Drupal\archimedes_client\Report;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for Archimedes Client routes.
 */
class ArchimedesClientController extends ControllerBase {

  /**
   * Returns the reporting status and data of the client.
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function adminStatus() {
    $config = $this->config('archimedes_client.settings');

    // Get the reporting method.
    $m = $config->get('server.method');
    switch ($m) {
      case 'http':
        $url = $config->get('server.url');
        $method = "HTTP - posted to $url";
        break;

      case 'email':
      default:
        $email = $config->get('server.email');
        $method = "Email - sent to $email";
        break;
    }

    // Get the report frequency.
    $interval = $config->get('cron.interval');
    if ($interval > 86400) {
      $n = $interval / 86400;
      $freq = ($n == 1) ? 'day' : "$n days";
    }
    elseif ($interval > 3600) {
      $n = $interval / 3600;
      $freq = ($n == 1) ? 'hour' : "$n hours";
    }
    else {
      $n = $interval;
      $freq = ($n == 1) ? 'second' : " $n seconds";
    }

    // Get the last report time.
    $last_run = \Drupal::state()->get('archimedes_client.last_report', 0);
    $last_d = date('r', $last_run);
    $last = ($last_run == 0) ? 'No reports have been sent yet!' : "The last report was sent at $last_d";

    // Get the next report time.
    $next_run = $last_run + $interval;
    $next_d = date('r', $next_run);
    $next = ($last_run == 0) ? 'as soon as possible' : "after $next_d";

    // Fetch rendered report items.
    $report = new Report();
    $data = $report->getRendered();

    // Generate table items.
    $rows = [];
    foreach ($data as $k => $v) {
      $rows[] = [$k, [
          'data' => $v,
          'id' => $k,
          'class' => 'report-field',
        ],
      ];
    }

    // Build the page.
    $build = [
      'method' => [
        '#markup' => '<b>Report Method</b>' . "<p>$method</p>",
      ],
      'Frequency' => [
        '#markup' => '<b>Report Frequency</b>' . "<p>Reports sent every $freq</p>",
      ],
      'LastReport' => [
        '#markup' => '<b>Last Report</b>' . "<p>$last</p>",
      ],
      'NextReport' => [
        '#markup' => '<b>Next Report</b>' . "<p>Next report will be run $next</p>",
      ],
      'report' => [
        '#type' => 'table',
        '#caption' => t('<b>Report Data</b>'),
        '#header' => ['Item', 'Value'],
        '#rows' => $rows,
        '#attributes' => ['id' => 'report'],
      ],
      'view' => [
        '#theme' => 'item_list',
        '#title' => t('View as'),
        '#items' => [
          [
            '#type' => 'link',
            '#url' => Url::fromRoute('archimedes_client.adminReport'),
            '#title' => t('Raw JSON'),
          ],
          [
            '#type' => 'link',
            '#url' => Url::fromRoute('archimedes_client.adminReport', ['pretty' => 1]),
            '#title' => t('Pretty JSON'),
          ],
        ],
      ],
      'settings' => [
        '#type' => 'link',
        '#url' => Url::fromRoute('archimedes_client.adminSettings'),
        '#title' => t('Change Archimedes Client settings...'),
      ],
    ];

    return $build;
  }

  /**
   * Returns a JSON encoded report.
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function adminReport() {

    // Create a report instance.
    $report = new Report();

    // Set JSON mime type.
    header('Content-Type: application/json');

    // Determine if pretty-print required or not, and output.
    $pretty = isset($_GET['pretty']);
    echo $report->getJSON($pretty);

    exit;
  }

}
