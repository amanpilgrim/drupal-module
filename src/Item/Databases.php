<?php

namespace Drupal\archimedes_client\Item;

use Drupal\Core\Database\Database;
use Drupal\archimedes_client\Item;

/**
 * Databases.
 *
 * Databases currently defined in the Drupal settings.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Databases extends Item {

  /**
   * Gets an array of the databases, keyed by connecction and category.
   *
   * @return array
   *   Databases
   */
  public function get() {
    $databases = Database::getAllConnectionInfo();
    $dbs = [];

    foreach ($databases as $category => $connections) {
      foreach ($connections as $name => $connection) {
        $dbs[$category][$name] = [
          'Driver' => $connection['driver'],
          'Database' => $connection['database'],
          'Host' => $connection['host'],
        ];
      }
    }
    return $dbs;
  }

  /**
   * Gets a string denoting the number of databases in each category.
   *
   * @return string
   *   Rendered markup
   */
  public function render() {
    $text = '';
    foreach ($this->get() as $category => $connections) {
      $c = count($connections);
      $p = ($c == 1) ? '' : 's';

      if (!empty($text)) {
        $text .= ' ,';
      }
      $text .= "$c $category connection{$p}";
    }
    return $text;
  }

}
