<?php

namespace Drupal\archimedes_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Contains \Drupal\archimedes_client\Form\ArchimedesClientSettingsForm.
 */

/**
 * Defines a form to configure maintenance settings for this site.
 */
class ArchimedesClientSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'archimedes_client_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['archimedes_client.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('archimedes_client.settings');

    $form['report_method'] = [
      '#type' => 'fieldset',
      '#title' => t('Reporting Method'),
      '#description' => t('Archimedes can be configured to send reports to the server over different channels. Reports can be emailed as attachments to a mailbox monitored by the server, or posted directly to the server over HTTP. All reporting methods are encrypted.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['report_method']['archimedes_client_server_method'] = [
      '#type' => 'radios',
      '#title' => t('Reporting Method'),
      '#default_value' => $config->get('server.method'),
      '#options' => [
        'email' => t('Email'),
        'http' => t('HTTP'),
      ],
    ];
    $form['report_method']['archimedes_client_server_email'] = [
      '#type' => 'textfield',
      '#title' => t('Server email address'),
      '#default_value' => $config->get('server.email'),
      '#description' => t('Email address of a mailbox monitored by the Archimedes server.'),

      /* Only visible when 'method' set to Email */
      '#states' => [
        'visible' => [
          ':input[name=archimedes_client_server_method]' => ['value' => 'email'],
        ],
      ],
    ];
    $form['report_method']['archimedes_client_server_url'] = [
      '#type' => 'textfield',
      '#title' => t('Server URL'),
      '#default_value' => $config->get('server.url'),
      '#description' => t("URL of the Archimedes server's HTTP post endpoint"),

      /* Only visible when 'method' set to HTTP */
      '#states' => [
        'visible' => [
          ':input[name=archimedes_client_server_method]' => ['value' => 'http'],
        ],
      ],
    ];
    $form['report_frequency'] = [
      '#type' => 'fieldset',
      '#title' => t('Reporting Frequency'),
      '#description' => t('Reports are sent to the server periodically, trigged by Drupal\'s cron runs. On each cron run, Archimedes will check to see if the interval since the last report exceeds this configured interval, and if so it will send the next report.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['report_frequency']['archimedes_client_cron_interval'] = [
      '#type' => 'textfield',
      '#title' => t('Reporting Interval (seconds)'),
      '#description' => t('Number of seconds between Archimedes reports. This is lower bound by the frequency Drupal\'s cron is run'),
      '#size' => 5,
      '#default_value' => $config->get('cron.interval'),
    ];
    $form['security'] = [
      '#type' => 'fieldset',
      '#title' => t('Security'),
      '#description' => t('Archimedes uses a public/private key pair to encrypt data during transmission (to prevent eavesdropping). Encrypted reports include an expiry time in order to minimise the effect of a reply attack.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['security']['archimedes_client_crypto_pubkey'] = [
      '#type' => 'textarea',
      '#title' => t('Public Key'),
      '#default_value' => $config->get('crypto.pubkey'),
      '#description' => t('The public key used to encrypt the reports sent to the server.'),
      '#element_validate' => [[get_class($this), 'pubkeyValidate']],
    ];
    $form['security']['archimedes_client_report_expiry'] = [
      '#type' => 'textfield',
      '#title' => t('Report Expiry Time (seconds)'),
      '#default_value' => $config->get('report.expiry'),
      '#size' => 5,
      '#description' => t('Number of seconds after sending that a report becomes invalid. The server must have enough time to receive the report before it expires, however the expiry should be set to a minimal value to minimise the effect of a reply attack.'),
    ];
    $form['customization'] = [
      '#type' => 'fieldset',
      '#title' => t('Customizations'),
      '#description' => t('Add custom system information to include with the standard reports.'),
      '#collabsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['customization']['archimedes_client_custom_fields'] = [
      '#type' => 'textarea',
      '#title' => t('Custom Fields'),
      '#default_value' => $config->get('settings.fields'),
      '#description' => t('Fill with the name of a Setting, based on settings values from the settings.php file, separated with a comma.'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * Ensure openssl can parse public key field.
   */
  public static function pubkeyValidate(&$element, FormStateInterface $form_state) {
    $key = isset($element['#value']) ? $element['#value'] : $element['#default_value'];
    if (!empty($key) && !openssl_pkey_get_public($key)) {
      $form_state->setError($element, t('The public key you provided is invalid. Reason: %errmsg', ['%errmsg' => openssl_error_string()]));
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('archimedes_client.settings')
      ->set('server.method', $form_state->getValue('archimedes_client_server_method'))
      ->set('server.email', $form_state->getValue('archimedes_client_server_email'))
      ->set('server.url', $form_state->getValue('archimedes_client_server_url'))
      ->set('cron.interval', $form_state->getValue('archimedes_client_cron_interval'))
      ->set('crypto.pubkey', $form_state->getValue('archimedes_client_crypto_pubkey'))
      ->set('report.expiry', $form_state->getValue('archimedes_client_report_expiry'))
      ->set('settings.fields', $form_state->getValue('archimedes_client_custom_fields'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
