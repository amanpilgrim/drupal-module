<?php

namespace Drupal\archimedes_client\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\archimedes_client\Report;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush commands to execute Archimedes tasks from commandline.
 */
class ArchimedesClientCommands extends DrushCommands {

  const CONFIG = 'archimedes_client.settings';

  protected $moduleConfig;

  /**
   * Constructs a new ArchimedesClientCommands object.
   * 
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration object factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->moduleConfig = $configFactory->get(self::CONFIG);
  }

  /**
   * Display an Archimedes Client report.
   *
   * @option format One of: table (default), json, json-pretty, pretty-json, encrypted, encrypted-json, ejson.
   * @usage drush archimedes:report
   *   Display a report.
   * @usage drush arch-report --format=json
   *   Display a report in JSON format.
   * @usage drush arch-report --format=encrypted > report.enc
   *   Save an encypted report to file.
   * @table-style default
   * @field-labels
   *   item: Item
   *   value: Value
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *
   * @command archimedes:report
   * @aliases arch-report,arch:report
   */
  public function report($options = ['format' => 'table']) {
    $rows = [];
    $report = new Report();
    switch ($options['format']) {
      case 'table':

      default:
        $data = $report->getRendered();
        foreach ($data as $item => $value) {
          $rows[] = [
            'item' => $item,
            'value' => $value,
          ];
        }
        break;

      case 'json':
        $this->output()->writeln($report->getJSON());
        return;

      case 'json-pretty':
      case 'pretty-json':
        $this->output()->writeln($report->getJSON(TRUE));
        return;

      case 'encrypted':
      case 'encrypted-json':
      case 'ejson':
        $this->output()->writeln($report->getEncrypted());
        return;
    }

    $result = new RowsOfFields($rows);
    return $result;
  }

  /**
   * Send an Archimedes Client report.
   *
   * @option method
   *   Reporting method to use (e.g. Email or HTTP).
   * @option location
   *   Email address or URL to send the report to (depending on chosen method).
   * @usage drush arch-send
   *   Send a report using the configured method.
   * @usage drush arch-send --method=http
   *   Send a report using HTTP.
   *
   * @command archimedes:send
   * @aliases arch-send,arch:send
   */
  public function send(array $options = ['method' => NULL, 'location' => NULL]) {
    if (empty($options['method'])) {
      $options['method'] = $this->moduleConfig->get('server.method');
    }

    if (empty($options['location'])) {
      switch ($options['method']) {
        case 'email':
          $options['location'] = $this->moduleConfig->get('server.email');
          break;

        case 'http':
          $options['location'] = $this->moduleConfig->get('server.url');
          break;
      }
    }

    $this->output()->writeln(dt('Sending "{method}" report to "{location}"...', $options));

    $report = new Report();
    $status = $report->send($options['method'], $options['location']);

    if ($status === TRUE) {
      $this->output()->writeln('Report successfully sent.');
    }
    else {
      $this->logger()->error('Could not send report via "{method}" method. Reason: {msg}.',
        [
          'method' => $options['method'],
          'msg' => $status,
        ]
      );
    }
  }

  /**
   * @hook validate archimedes:send
   */
  public function validateArchimedes(CommandData $commandData) {

    if (empty($this->input->getOption('uri'))) {
      throw new \Exception('Please specify valid hostname for this site( [-l|--uri URI] ).');
    }

    $method = $this->input->getOption('method');
    if (!empty($method) && !in_array($method, ['email', 'http'], TRUE)) {
      throw new \Exception('Unsupported method.');
    }
  }

}
